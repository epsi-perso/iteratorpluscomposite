/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.interatorpluscomposite;

/**
 *
 * @author TVall
 */
public class CandyStore {
    private Candy[] candies;
    
        public CandyStore(int size){
            candies = new Candy[size];
            for (int i = 0; i < size; i++){
                candies[i] = new Candy("candy" + i);
            }
    }
    
    public Iterator iterator() {
        return new CandyStoreIterator(candies);
    }
}
