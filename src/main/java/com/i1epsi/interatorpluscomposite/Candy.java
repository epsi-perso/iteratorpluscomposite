/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.i1epsi.interatorpluscomposite;

/**
 *
 * @author TVall
 */
public class Candy {

    private String name;

    public Candy(String name){
        this.name = name;
    }
    
    @Override
    public String toString() {
        return "Candy{" + "name='" + name +'}';
    }
    
    
}
