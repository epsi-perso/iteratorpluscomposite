/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.interatorpluscomposite;

/**
 *
 * @author TVall
 */
public class CandyStoreIterator implements Iterator{
    private Candy[] candies;
    private int position;

    public CandyStoreIterator(Candy[] candies, int position) {
        this.candies = candies;
        this.position = 0;
    }
    
    
    
    @Override
    public boolean hastNext() {
        return this.position < this.candies.length; 
    }
    
    @Override
    public Object next() {
        Candy candy = this.candies[this.position];
        this.position++;
        return candy;
    }  
     
}
